module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  publicPath: process.env.NODE_ENV === 'production' ? '/innovalab-admin/' : '/',

  pluginOptions: {
    cordovaPath: 'src-cordova'
  }
}


